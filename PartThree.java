import java.util.Scanner;

public class PartThree{
	public static void main(String[] args){
		AreaComputations sc = new AreaComputations();
		Scanner reader = new Scanner(System.in);
		System.out.println("Please input a value for the length of a square:");
		double squareLength = reader.nextDouble();
		System.out.println("Please input a value for the length of a rectangle:");
		double rectLenght = reader.nextDouble();
		System.out.println("Please input a value for the width of a rectangle:");
		double rectWidth= reader.nextDouble();
		
		double areaSquare = AreaComputations.areaSquare(squareLength);
		double areaRectangle = sc.areaRectangle(rectLenght, rectWidth);
		System.out.println(areaSquare);
		System.out.println(areaRectangle);
	}
}