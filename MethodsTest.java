public class MethodsTest{
	public static void main(String[] args){
		int x = 10;
		//4. methodNoInputNoReturn
		System.out.println(x);
		methodNoInputNoReturn();
		
		//5. methodOneInputNoReturn
		System.out.println(x);
		methodOneInputNoReturn(10);
		methodOneInputNoReturn(x);
		methodOneInputNoReturn(x+50);
		
		//6. methodTwoInputNoReturn
		methodTwoInputNoReturn(3, 3.5);
		
		//7. methodNoInputReturnInt
		int z = methodNoInputReturnInt();
		System.out.println(z);
		
		//8. sumSquareRoot
		double root = sumSquareRoot(6, 3);
		System.out.println(root);
		
		//10. instance method String
		String s1 = "hello";
		String s2 = "goodbye";
		System.out.println(s1.length());
		System.out.println(s2.length());
		
		//11. calling instance and static 
		System.out.println(SecondClass.addOne(50));
		SecondClass sc = new SecondClass();
		System.out.println(sc.addTwo(50));
	}
  
	public static void methodNoInputNoReturn(){
		int x = 50;
		System.out.println(x);
		System.out.println("I’m in a method that takes no input and returns nothing");
	}
  
	public static void methodOneInputNoReturn(int input1){
		System.out.println("Inside the method one input no return");
		System.out.println(input1);
	}
	
	public static void methodTwoInputNoReturn(int value1, double value2){
		System.out.println(value1 + " and " + value2);
	}
	
	public static int methodNoInputReturnInt(){
		return 6;
	}
	
	public static double sumSquareRoot(int num1, int num2){
		double root = num1 + num2;
		return Math.sqrt(root);
	}
}