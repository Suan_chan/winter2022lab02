public class AreaComputations{
	public static double areaSquare(double squareLength){
		return Math.pow(squareLength, 2);
	}
	
	public double areaRectangle(double rectLength, double rectWidth){
		return rectLength * rectWidth;
	}
}